<?php 
  class Poneys {
      private $count = 8;
      const MAX = 15;

      public function getCount() {
        return $this->count;
      }

      public function setCount($a) {
        $this->count = $a;
      }

      public function removePoneyFromField($number) {
        $this->count -= $number;        
      }

      public function removePoneyWithException($number) {
        if ($number > $this->count)
          throw new Exception("Suppresion de plus de poneys qu'il y'en a!", 1);
        else
          $this->count -= $number;        
      }

      public function addPoney($number) {
        $this->count += $number;
      }

      public function placesRestantes(){
        if ($this->count < self::MAX)
          return true;
        else 
          return false;
      }

      public function getNames() {

      }
  }
?>
