<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    private $Poneys;

    public function setUp() {
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(8);
    }

    public function test_removePoneyFromField() {
      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());
    }

    public function test_removePoneyWithException() {
      // Action
      try {
        $this->Poneys->removePoneyWithException(9);
      } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
      }
      
      // Assert
      $this->assertEquals(8, $this->Poneys->getCount());
    }

    public function test_addPoney() {
      // Action
      $this->Poneys->addPoney(3);
      
      // Assert
      $this->assertEquals(11, $this->Poneys->getCount());
    }

    /**
     * @dataProvider removeProvider
     */
    public function test_removePoneyWithProvider($a, $b) {
      // Action
      try {
        $this->Poneys->removePoneyWithException($a);
      } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
      }
      
      // Assert
      $this->assertEquals($b, $this->Poneys->getCount());
    }
    public function removeProvider()
    {
        return [
            [0, 8],
            [3, 5],
            [6, 2],
            [9, 8]
        ];
    }

    public function test_getNamesViaMock() {
      $this->Poneys = $this->getMockBuilder('Poneys')->getMock();                       //creation du Mock
      $this->Poneys->method('getNames')->willReturn(array('toto', 'titi', 'tata'));     //Simuler l'execution de la fonction

      $this->assertContains('tata', $this->Poneys->getNames());
    }

    public function test_placesRestantes() {
      $this->assertTrue($this->Poneys->placesRestantes());
    }

    public function tearDown() {
      unset($this->Poneys);
    }
  }
 ?>
